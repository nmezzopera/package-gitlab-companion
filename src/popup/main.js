import Vue from "vue";
import App from "./App.vue";
import { GlToast } from "@gitlab/ui";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faEdit, faCopy } from "@fortawesome/free-solid-svg-icons";
import { faSlack, faGitlab } from "@fortawesome/free-brands-svg-icons";

library.add(faEdit, faCopy, faSlack, faGitlab);

Vue.component("fa", FontAwesomeIcon);

Vue.use(GlToast);

/* eslint-disable no-new */
new Vue({
  el: "#app",
  render: h => h(App)
});
